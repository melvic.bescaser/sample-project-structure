jest.mock('./database/connection', () => {
  const original = jest.requireActual('./database/connection').default;

  Object.assign(original, {
    beginTransaction: jest.fn(),
    commitTransaction: jest.fn(),
    rollbackTransaction: jest.fn(),
    connect: jest.fn(),
    disconnect: jest.fn(),
    query: jest.fn(),
    getDatabaseName: jest.fn().mockReturnValue('test'),
  });

  return {
    __esModule: true,
    default: original,
  };
});
