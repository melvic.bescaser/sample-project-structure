import getTransaction from './getTransaction';
import updateTransaction from './updateTransaction';

export default {
  getTransaction,
  updateTransaction,
};
