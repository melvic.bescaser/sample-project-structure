import transaction from '../../../database/transaction';
import repositories from '../repositories';

const getTransaction = async (transactionId) => {
  try {
    const result = await transaction(async () => {
      const transaction = await repositories.getTransactionById(transactionId);
      const count = await repositories.countTransactions();

      return { count, transaction };
    });

    return result;
  } catch (error) {
    throw error;
  }
};

export default getTransaction;
