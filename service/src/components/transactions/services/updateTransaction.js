import transaction from '../../../database/transaction';
import repositories from '../repositories';
import services from './';

const updateTransaction = async (transactionId) => {
  try {
    const result = await transaction(async () => {
      const transaction = await repositories.getTransactionById(transactionId);
      const count = await repositories.countTransactions();

      const getTransaction = await services.getTransaction(transactionId);

      return { count, transaction, getTransaction: getTransaction };
    });

    return result;
  } catch (error) {
    throw error;
  }
};

export default updateTransaction;
