import response from '../../../commons/response';
import services from '../services';

const getTransaction = async (event) => {
  try {
    const result = await services.getTransaction(event.transactionId);

    return response.handleResponse(result);
  } catch (error) {
    console.log(error.stack);
    return response.handleResponseError(error);
  }
};

export default getTransaction;
