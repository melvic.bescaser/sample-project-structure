import countTransactions from './countTransactions';
import getTransactionById from './getTransactionById';

export default {
  countTransactions,
  getTransactionById,
};
