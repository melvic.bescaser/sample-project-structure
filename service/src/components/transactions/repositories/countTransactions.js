import connection from '../../../database/connection';

const countTransactions = async () => {
  try {
    const result = await connection.query({
      name: 'count-transactions',
      text: `SELECT COUNT(*) as count FROM ${connection.getDatabaseName()}.transactions`
    });

    return result.rows[0].count;
  } catch (error) {
    // TODO: log eror
    throw error;
  }
};

export default countTransactions;
