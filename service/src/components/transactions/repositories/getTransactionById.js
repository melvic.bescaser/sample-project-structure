import connection from '../../../database/connection';

const getTransactionById = async (transactionId) => {
  try {
    const result = await connection.query({
      name: 'get-transaction-by-id',
      text: `SELECT t.* FROM ${connection.getDatabaseName()}.transactions t WHERE t.id = $1`,
      values: [transactionId]
    });

    if (result.rows.length === 0) {
      throw new Error('Empty transaction.');
    }

    return result.rows[0];
  } catch (error) {
    // TODO: log eror
    throw error;
  }
};

export default getTransactionById;
