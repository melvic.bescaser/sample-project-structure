const composeAsync = (...functions) => (input) =>
  functions.reduceRight(
    (chain, func) => chain.then(func),
    Promise.resolve(input),
  );

export default composeAsync;
