const handleResponse = (data) => {
  return {
    status: 'success',
    data,
  };
};

const handleResponseError = (error) => {
  return {
    status: 'error',
    message: error.message,
  };
};

export default {
  handleResponse,
  handleResponseError,
};
