import { performance } from 'perf_hooks';
import transactionsControllers from '../components/transactions/controllers';

const routes = {
  ...transactionsControllers,
};

const main = async (event) => {
  const start = performance.now();
  const result = await routes.getTransaction(event);
  const end = performance.now();

  console.log(`Time elapsed: ${end - start}ms`);

  return {
    message: 'Go Serverless v1.0! Your function executed successfully!',
    ...result,
  };
};

export default main;
