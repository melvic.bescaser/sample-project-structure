import ServerlessPostgresConnection from './ServerlessPostgresConnection';
import PostgresConnection from './PostgresConnection';
import PostgresPoolConnection from './PostgresPoolConnection';

const createConnection = (type, config) => {
  switch (type) {
    case 'postgres':
      return new PostgresConnection(config);
    case 'postgres:pool':
      return new PostgresPoolConnection(config);
    case 'serverless-postgres':
    default:
      return new ServerlessPostgresConnection(config);
  }
};

export default createConnection;
