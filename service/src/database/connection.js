import createConnection from './createConnection';

const connection = createConnection(process.env.DB_TYPE, {
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,
});

const logQuery = (params) => {
  if (process.env.STAGE_NAME !== 'local') {
    return params;
  }

  console.log(...params);

  return params;
};

connection.setQueryWrappers(logQuery);

export default connection;
