import { Pool } from 'pg';

import DatabaseConnection from './DatabaseConnection';

const defaultConfig = {
  max: 20,
  idleTimeoutMillis: 30000,
};

class PostgresPoolConnection extends DatabaseConnection {
  pool;

  connection;

  transactionStack = 0;

  constructor(config) {
    super(config.database);
    this.pool = new Pool({ ...defaultConfig, ...config });
  }

  async connect() {
    this.connection = await this.pool.connect();

    return this.connection;
  }

  async disconnect() {
    return await this.connection.release();
  }

  async query(...params) {
    const isQueryInTransactionFlags = DatabaseConnection.checkIsQueryInTransactionFlags(
      params
    );

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.connect();
    }

    const newParams = await super.query(...params);
    const result = await this.connection.query(...newParams);

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.disconnect();
    }

    return result;
  }

  async beginTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.BEGIN);
  }

  async commitTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.COMMIT);
  }

  async rollbackTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.ROLLBACK);
  }
}

export default PostgresPoolConnection;
