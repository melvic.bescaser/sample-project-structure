import { Client } from 'pg';

import DatabaseConnection from './DatabaseConnection';

const defaultConfig = {
  max: 20,
  idleTimeoutMillis: 30000,
};

class PostgresConnection extends DatabaseConnection {
  connection;

  transactionStack = 0;

  constructor(config) {
    super(config.database);
    this.connection = new Client({ ...defaultConfig, ...config });
  }

  async connect() {
    return await this.connection.connect();
  }

  async disconnect() {
    return await this.connection.end();
  }

  async query(...params) {
    const isQueryInTransactionFlags = DatabaseConnection.checkIsQueryInTransactionFlags(
      params
    );

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.connect();
    }

    const newParams = await super.query(...params);
    const result = await this.connection.query(...newParams);

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.disconnect();
    }

    return result;
  }

  async beginTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.BEGIN);
  }

  async commitTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.COMMIT);
  }

  async rollbackTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.ROLLBACK);
  }
}

export default PostgresConnection;
