import ServerlessClient from 'serverless-postgres';

import DatabaseConnection from './DatabaseConnection';

const defaultConfig = {
  max: 20,
  idleTimeoutMillis: 30000,
};

class ServerlessPostgresConnection extends DatabaseConnection {
  connection;

  transactionStack = 0;

  constructor(config) {
    super(config.database, [
      ServerlessPostgresConnection.addSuffixInQueryConfigName,
    ]);
    this.connection = new ServerlessClient({ ...defaultConfig, ...config });
  }

  static addSuffixInQueryConfigName(params) {
    if (params.length !== 1) {
      return params;
    }

    const [param, ...restOfParams] = params;

    if (!(typeof param === 'object' && param.name)) {
      return params;
    }

    return [{ ...param, name: `${param.name}-${Date.now()}` }, ...restOfParams];
  }

  async connect() {
    await this.connection.connect();
  }

  async disconnect() {
    await this.connection.clean();
  }

  async query(...params) {
    const isQueryInTransactionFlags = DatabaseConnection.checkIsQueryInTransactionFlags(
      params
    );

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.connect();
    }

    const newParams = await super.query(...params);
    const result = await this.connection.query(...newParams);

    if (this.getTransactionStack() === 0 && !isQueryInTransactionFlags) {
      await this.disconnect();
    }

    return result;
  }

  async beginTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.BEGIN);
  }

  async commitTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.COMMIT);
  }

  async rollbackTransaction() {
    await this.query(DatabaseConnection.TRANSACTION_FLAGS.ROLLBACK);
  }

  getTransactionStack() {
    return this.transactionStack;
  }

  setTransactionStack(transactionStack) {
    this.transactionStack = transactionStack;
  }
}

export default ServerlessPostgresConnection;
