import composeAsync from '../commons/composeAsync';

class DatabaseConnection {
  databaseName;

  queryWrappers;

  static TRANSACTION_FLAGS = Object.freeze({
    BEGIN: 'BEGIN',
    COMMIT: 'COMMIT',
    ROLLBACK: 'ROLLBACK',
  });

  constructor(databaseName, queryWrappers = []) {
    this.databaseName = databaseName;
    this.queryWrappers = queryWrappers;
  }

  static checkIsQueryInTransactionFlags(params) {
    if (params.length !== 1) {
      return false;
    }

    const param = params[0];

    if (typeof param !== 'string') {
      return false;
    }

    return Object.values(DatabaseConnection.TRANSACTION_FLAGS).includes(param);
  }

  getDatabaseName() {
    return this.databaseName;
  }

  setQueryWrappers(...queryWrappers) {
    this.queryWrappers.unshift(...queryWrappers);
  }

  query(...params) {
    return composeAsync(...this.queryWrappers)(params);
  }
}

export default DatabaseConnection;
