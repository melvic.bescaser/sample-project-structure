import connection from './connection';

const transaction = async (scope) => {
  if (connection.getTransactionStack() === 0) {
    await connection.connect();
  }

  try {
    if (connection.getTransactionStack() === 0) {
      await connection.beginTransaction();
    }

    connection.setTransactionStack(connection.getTransactionStack() + 1);

    const result = await scope(connection);

    if (connection.getTransactionStack() === 1) {
      await connection.commitTransaction();
    }

    connection.setTransactionStack(connection.getTransactionStack() - 1);

    return result;
  } catch (error) {
    if (connection.getTransactionStack() === 1) {
      await connection.rollbackTransaction();
    }

    connection.setTransactionStack(connection.getTransactionStack() - 1);

    // TODO: log eror
    throw error;
  } finally {
    if (connection.getTransactionStack() === 0) {
      await connection.disconnect();
    }
  }
};

export default transaction;
