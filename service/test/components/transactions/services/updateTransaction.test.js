import connection from '../../../../src/database/connection';
import repositories from '../../../../src/components/transactions/repositories';
import services from '../../../../src/components/transactions/services';
import updateTransaction from '../../../../src/components/transactions/services/updateTransaction';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should pass', async () => {
  const spyGetTransactionById = jest.spyOn(repositories, 'getTransactionById').mockImplementation(() => Promise.resolve({ id: '123' }));
  const spyCountTransactions = jest.spyOn(repositories, 'countTransactions').mockImplementation(() => Promise.resolve(999));
  const spyGetTransaction = jest.spyOn(services, 'getTransaction').mockImplementation(() => Promise.resolve({ transaction: { id: '345' }, count: 1000  }));

  await expect(updateTransaction('123')).resolves.toMatchObject({
    transaction: { id: '123' },
    count: 999,
    getTransaction: {},
  });
  expect(spyGetTransactionById).toHaveBeenCalledWith('123');
  expect(spyCountTransactions).toHaveBeenCalled();
  expect(spyGetTransaction).toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.commitTransaction).toHaveBeenCalledTimes(1);
  expect(connection.rollbackTransaction).toHaveBeenCalledTimes(0);
});

it('should fail', async () => {
  const spyGetTransactionById = jest.spyOn(repositories, 'getTransactionById').mockImplementation(() => Promise.reject(new Error('Repo `getTransactionById` error.')));
  const spyCountTransactions = jest.spyOn(repositories, 'countTransactions').mockImplementation(() => Promise.resolve(999));
  const spyGetTransaction = jest.spyOn(services, 'getTransaction').mockImplementation(() => Promise.resolve({ transaction: { id: '345' }, count: 1000  }));

  await expect(updateTransaction('123')).rejects.toMatchObject(new Error('Repo `getTransactionById` error.'));
  expect(spyGetTransactionById).toHaveBeenCalledWith('123');
  expect(spyCountTransactions).not.toHaveBeenCalled();
  expect(spyGetTransaction).not.toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.commitTransaction).toHaveBeenCalledTimes(0);
  expect(connection.rollbackTransaction).toHaveBeenCalledTimes(1);
});
