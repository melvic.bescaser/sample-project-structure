import connection from '../../../../src/database/connection';
import repositories from '../../../../src/components/transactions/repositories';
import getTransaction from '../../../../src/components/transactions/services/getTransaction';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should pass', async () => {
  const spyGetTransactionById = jest.spyOn(repositories, 'getTransactionById').mockImplementation(() => Promise.resolve({ id: '123' }));
  const spyCountTransactions = jest.spyOn(repositories, 'countTransactions').mockImplementation(() => Promise.resolve(999));

  await expect(getTransaction('123')).resolves.toMatchObject({
    transaction: { id: '123' },
    count: 999,
  });
  expect(spyGetTransactionById).toHaveBeenCalledWith('123');
  expect(spyCountTransactions).toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.commitTransaction).toHaveBeenCalledTimes(1);
});

it('should fail on get transaction by id', async () => {
  const spyGetTransactionById = jest.spyOn(repositories, 'getTransactionById').mockImplementation(() => Promise.reject(new Error('getTransactionById error.')));
  const spyCountTransactions = jest.spyOn(repositories, 'countTransactions').mockImplementation(() => Promise.resolve(999));

  await expect(getTransaction('123')).rejects.toMatchObject(new Error('getTransactionById error.'));

  expect(spyGetTransactionById).toHaveBeenCalledWith('123');
  expect(spyCountTransactions).not.toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.rollbackTransaction).toHaveBeenCalledTimes(1);
});

it('should fail on count transactions', async () => {
  const spyGetTransactionById = jest.spyOn(repositories, 'getTransactionById').mockImplementation(() => Promise.resolve({ id: '123' }));
  const spyCountTransactions = jest.spyOn(repositories, 'countTransactions').mockImplementation(() => Promise.reject(new Error('countTransactions error.')));

  await expect(getTransaction('123')).rejects.toMatchObject(new Error('countTransactions error.'));

  expect(spyGetTransactionById).toHaveBeenCalledWith('123');
  expect(spyCountTransactions).toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.rollbackTransaction).toHaveBeenCalledTimes(1);
});
