import connection from '../../../../src/database/connection';
import getTransactionById from '../../../../src/components/transactions/repositories/getTransactionById';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should pass', async () => {
  connection.query.mockImplementation(() => Promise.resolve({ rows: [{ name: 'hello' }] }));

  await expect(getTransactionById('123')).resolves.toMatchObject({ name: 'hello' });
  expect(connection.query).toHaveBeenCalledWith(expect.objectContaining({
    text: 'SELECT t.* FROM test.transactions t WHERE t.id = $1'
  }))
});

it('should fail', async () => {
  connection.query.mockImplementation(() => Promise.reject(new Error('DB error.')));

  await expect(getTransactionById('123')).rejects.toMatchObject(new Error('DB error.'));
});
