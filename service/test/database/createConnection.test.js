import createConnection from '../../src/database/createConnection';
import PostgresConnection from '../../src/database/PostgresConnection';
import PostgresPoolConnection from '../../src/database/PostgresPoolConnection';
import ServerlessPostgresConnection from '../../src/database/ServerlessPostgresConnection';

jest.mock('../../src/database/PostgresConnection');
jest.mock('../../src/database/PostgresPoolConnection');
jest.mock('../../src/database/ServerlessPostgresConnection');

beforeEach(() => {
  jest.clearAllMocks();
});

it('should pass with default type', () => {
  const connection = createConnection();

  expect(connection).toBeInstanceOf(ServerlessPostgresConnection);
});

it('should pass with given `postgres` type', () => {
  const connection = createConnection('postgres');

  expect(connection).toBeInstanceOf(PostgresConnection);
});

it('should pass with given `postgres:pool` type', () => {
  const connection = createConnection('postgres:pool');

  expect(connection).toBeInstanceOf(PostgresPoolConnection);
});

it('should pass with `serverless-postgres` type', () => {
  const connection = createConnection('serverless-postgres');

  expect(connection).toBeInstanceOf(ServerlessPostgresConnection);
});
