import connection from '../../src/database/connection';
import transaction from '../../src/database/transaction';

beforeEach(() => {
  jest.clearAllMocks();
});

it('should pass', async () => {
  const scopeFn = jest.fn().mockImplementation(() => Promise.resolve(true));
  await expect(transaction(scopeFn)).resolves.toEqual(true);

  expect(scopeFn).toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalled();
  expect(connection.commitTransaction).toHaveBeenCalled();
});

it('should pass and omit invocation of transaction flags', async () => {
  const scopeFn = jest.fn().mockImplementation(() => Promise.resolve(true));
  await expect(transaction(() => transaction(scopeFn))).resolves.toEqual(true);
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.commitTransaction).toHaveBeenCalledTimes(1);
});

it('should fail', async () => {
  const scopeFn = jest.fn().mockImplementation(() => Promise.reject(new Error('Some error')));
  await expect(transaction(scopeFn)).rejects.toEqual(new Error('Some error'));

  expect(scopeFn).toHaveBeenCalled();
  expect(connection.beginTransaction).toHaveBeenCalled();
  expect(connection.rollbackTransaction).toHaveBeenCalled();
});

it('should fail and omit invocation of transaction flags', async () => {
  const scopeFn = jest.fn().mockImplementation(() => Promise.reject(new Error('Some error')));

  await expect(transaction(() => transaction(scopeFn))).rejects.toEqual(new Error('Some error'));
  expect(connection.beginTransaction).toHaveBeenCalledTimes(1);
  expect(connection.rollbackTransaction).toHaveBeenCalledTimes(1);
});
