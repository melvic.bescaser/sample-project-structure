import Router from './helpers/router';
import transactionsRoutes from './components/transactions/routes';
import usersRoutes from './components/users/routes';

const router = new Router();

const middleware = () => ({
  before: async () => {
    console.log('main middleware');
  },
});

router.use(middleware());
router.get('/', () => ({ statusCode: 200, body: JSON.stringify({ hello: 'world', method: 'get' }) }));
router.use('/transactions', transactionsRoutes);
router.use('/users', usersRoutes);

export default router;
