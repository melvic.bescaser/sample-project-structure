export const STATUS_CODES = {
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500,
};

export const RESPONSE_STATUS = {
  SUCCESS: 'success',
  ERROR: 'error',
};

export const ERROR_STATUS_CODES = {
  [STATUS_CODES.INTERNAL_SERVER_ERROR]: ['1-0001', '1-1002'],
  [STATUS_CODES.NOT_FOUND]: ['1-0003', '1-1004'],
};

export const HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  PATCH: 'PATCH',
  DELETE: 'DELETE',
};
