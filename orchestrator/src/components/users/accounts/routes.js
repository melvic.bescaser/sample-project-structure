import Router from '../../../helpers/router';

const router = new Router();

const middleware = () => ({
  before: async (request) => {
    console.log(request.internal);
  },
});

const accountsMiddleware = () => ({
  before: async () => {
    console.log('This is accounts middleware.');
  },
});

router.use(accountsMiddleware());

router.get(
  '/',
  middleware(),
  () => ({ statusCode: 200, body: JSON.stringify({ desc: 'accounts' }) }),
);

router.get(
  '/{accountId}',
  middleware(),
  () => ({ statusCode: 200, body: JSON.stringify({ desc: 'accounts with accountId' }) }),
);

export default router;
