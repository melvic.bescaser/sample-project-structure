import Router from '../../helpers/router';

import accountsRoutes from './accounts/routes';

const router = new Router();

const middleware = () => ({
  before: async (request) => {
    console.log(request.internal);
  },
});

const usersMiddleware = () => ({
  before: async () => {
    console.log('This is users middleware.');
  },
});

router.use(usersMiddleware());

router.get('/', middleware(), () => ({
  statusCode: 200,
  body: JSON.stringify({
    desc: 'users without id controller',
  }),
}));

router.get('/{userId}', middleware(), () => ({
  statusCode: 200,
  body: JSON.stringify({
    desc: 'user with id controller',
  }),
}));

router.use('/{userId}/accounts', accountsRoutes);

export default router;
