import invokeFunction from '../../infrastructures/invokeFunction';
import { RESPONSE_STATUS } from '../../config/constants';
import FunctionInvocationResponseError from '../../errors/FunctionInvocationResponseError';

const getTransactionHeader = (body) => {
  const result = invokeFunction(process.env.TRANSACTIONS_SERVICE, { action: 'getTransactionHeader', body });

  if (result.status === RESPONSE_STATUS.ERROR) {
    throw new FunctionInvocationResponseError(result.data);
  }

  return {
    transactionId: 'some-transaction-id',
    buyerReferenceNumber: 'some-buyer-reference-number',
  };
  // return result.data;
};

export default {
  getTransactionHeader,
};
