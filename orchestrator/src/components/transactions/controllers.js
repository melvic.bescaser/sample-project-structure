import response from '../../helpers/response';
import services from './services';

const getTransactionHeader = async (event/* context */) => {
  try {
    const { pathParameters: { transactionId } } = event;
    const result = await services.getTransactionHeader({ transactionId });

    return response.handleResponse(result);
  } catch (error) {
    return response.handleResponseError(error);
  }
};

export default {
  getTransactionHeader,
};
