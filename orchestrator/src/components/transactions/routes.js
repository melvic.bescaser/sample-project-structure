import Router from '../../helpers/router';
// import controllers from './controllers';

const router = new Router();

const middleware = () => ({
  before: async (request) => {
    console.log(request.internal);
  },
});

const transactionsMiddleware = () => ({
  before: async () => {
    console.log('This is transactions middleware.');
  },
});

router.use(transactionsMiddleware());

// router.get('/{transactionId}', middleware(), controllers.getTransactionHeader);
router.get('/', middleware(), () => ({
  statusCode: 200,
  body: JSON.stringify({
    desc: 'transactions without id controller',
  }),
}));

router.get('/{transactionId}', middleware(), () => ({
  statusCode: 200,
  body: JSON.stringify({
    desc: 'transaction with id controller',
  }),
}));

export default router;
