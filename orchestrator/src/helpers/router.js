import middy from '@middy/core';
import { getInternal } from '@middy/util';
import { HTTP_METHODS } from '../config/constants';
import NotFoundError from '../errors/NotFoundError';

const validHttpMethods = Object.values(HTTP_METHODS);
const validMiddlewareObjectKeys = ['before', 'after', 'onError'];

const addInternalsToContext = () => ({
  before: async (request) => {
    const internal = await getInternal(true, request);

    Object.assign(request.context, { internal });
  },
});

class Router {
  constructor() {
    this.routes = [];
  }

  route(path, method, ...handlers) {
    if (!validHttpMethods.includes(method)) {
      throw new Error(`Route method '${method}' is invalid.`);
    }

    const handlersLength = handlers.length;

    if (handlersLength === 0) {
      throw new Error('Route handlers should not be empty.');
    }

    const hasDuplicate = this.routes.some((route) => {
      if (!('method' in route)) {
        return false;
      }

      const isSamePath = route.path === path;

      if (isSamePath && route.method !== method) {
        return false;
      }

      return isSamePath;
    });

    if (hasDuplicate) {
      throw new Error('Route path should be unique.');
    }

    const controller = handlers.pop();
    const middlewares = handlers.reduce((middlewaresSoFar, middleware) => {
      middlewaresSoFar.push(...(Array.isArray(middleware) ? [...middleware] : [middleware]));

      return middlewaresSoFar;
    }, []);

    this.routes.push({
      path,
      method,
      middlewares,
      controller,
    });
  }

  get(path, ...handlers) {
    this.route(path, HTTP_METHODS.GET, ...handlers);
  }

  post(path, ...handlers) {
    this.route(path, HTTP_METHODS.POST, ...handlers);
  }

  put(path, ...handlers) {
    this.route(path, HTTP_METHODS.PUT, ...handlers);
  }

  patch(path, ...handlers) {
    this.route(path, HTTP_METHODS.PATCH, ...handlers);
  }

  delete(path, ...handlers) {
    this.route(path, HTTP_METHODS.DELETE, ...handlers);
  }

  use(...params) {
    const paramsLength = params.length;

    if (!(paramsLength > 0 && paramsLength <= 2)) {
      throw new Error('Invalid use case of `use` method.');
    }

    if (
      paramsLength === 1
      && typeof params[0] === 'object'
      && validMiddlewareObjectKeys.some((key) => key in params[0])
    ) {
      this.routes.push({
        path: '/',
        middlewares: [params[0]],
      });

      return undefined;
    }

    if (
      paramsLength === 2
      && typeof params[0] === 'string'
      && params[1] instanceof Router
    ) {
      this.routes.push({
        path: params[0],
        router: params[1],
      });

      return undefined;
    }

    return undefined;
  }

  getRoute(path, method, middlewares = []) {
    const routesLength = this.routes.length;

    for (let i = 0; i < routesLength; i += 1) {
      const route = this.routes[i];

      if (
        path.startsWith(route.path)
        && 'middlewares' in route
        && !('controller' in route)
        && !('router' in route)
      ) {
        middlewares.push(...route.middlewares);
      }

      if (
        path.startsWith(route.path)
        && 'router' in route
        && !('middlewares' in route)
        && !('controller' in route)
      ) {
        const response = route.router.getRoute(path.replace(route.path, '') || '/', method, middlewares);

        if (response) {
          return response;
        }
      }

      if (
        path === route.path
        && method === route.method
        && !('router' in route)
        && 'middlewares' in route
        && 'controller' in route
      ) {
        middlewares.push(...route.middlewares);

        const handler = middy(route.controller)
          .use(middlewares)
          .use(addInternalsToContext());

        return handler;
      }
    }

    throw new NotFoundError();
  }
}

export default Router;
