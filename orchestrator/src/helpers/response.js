import getErrorStatusCodeFromCode from '../utils/getErrorStatusCodeFromCode';
import FunctionInvocationResponseError from '../errors/FunctionInvocationResponseError';
import HttpResponseError from '../errors/HttpResponseError';
import InternalServerError from '../errors/InternalServerError';

const defaultHeaders = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Methods': 'GET,POST,PATCH,DELETE',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Headers': 'x-transaction-id,Content-Type,Authorization',
  'Access-Control-Allow-Credentials': true,
};

const handleResponse = (body, statusCode = 200, headers) => ({
  headers: {
    ...defaultHeaders,
    ...headers,
  },
  ...(body ? { statusCode, body: JSON.stringify(body) } : { statusCode: 204 }),
});

const createResponseError = (error) => {
  if (error instanceof FunctionInvocationResponseError) {
    return new HttpResponseError(getErrorStatusCodeFromCode(error.code), error.body);
  }

  if (error instanceof HttpResponseError) {
    return error;
  }

  return new InternalServerError();
};

const handleResponseError = (error, headers) => {
  const responseError = createResponseError(error);

  return {
    headers: {
      ...defaultHeaders,
      ...headers,
    },
    statusCode: responseError.statusCode,
    body: JSON.stringify(responseError.body),
  };
};

export default {
  handleResponse,
  handleResponseError,
  createResponseError,
};
