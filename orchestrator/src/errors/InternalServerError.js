import { STATUS_CODES } from '../config/constants';
import HttpResponseError from './HttpResponseError';

class InternalServerError extends HttpResponseError {
  constructor({ message = 'Something went wrong, please try again.', ...payload } = {}) {
    super(STATUS_CODES.INTERNAL_SERVER_ERROR, { ...payload, message });

    this.name = 'InternalServerError';
  }
}

export default InternalServerError;
