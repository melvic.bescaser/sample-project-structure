import { STATUS_CODES } from '../config/constants';
import HttpResponseError from './HttpResponseError';

class NotFoundError extends HttpResponseError {
  constructor({ message = 'The resource that you are trying to access does not exist.', ...payload } = {}) {
    super(STATUS_CODES.NOT_FOUND, { ...payload, message });

    this.name = 'NotFoundError';
  }
}

export default NotFoundError;
