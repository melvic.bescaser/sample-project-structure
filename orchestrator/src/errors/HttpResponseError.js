class HttpResponseError extends Error {
  constructor(statusCode, body = {}) {
    super(body.message);

    this.name = 'HttpResponseError';
    this.statusCode = statusCode;
    this.body = body;
  }
}

export default HttpResponseError;
