class FunctionInvocationResponseError extends Error {
  constructor(payload = {}) {
    super(payload.message);

    this.name = 'FunctionInvocationResponseError';
    this.body = payload;
  }
}

export default FunctionInvocationResponseError;
