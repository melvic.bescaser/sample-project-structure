import response from './helpers/response';
import routes from './routes';

// eslint-disable-next-line import/prefer-default-export
export const main = async (event, context) => {
  try {
    const {
      requestContext: { httpMethod, resourcePath },
    } = event;
    const route = routes.getRoute(resourcePath, httpMethod);

    return await route(event, context);
  } catch (error) {
    return response.handleResponseError(error);
  }
};
