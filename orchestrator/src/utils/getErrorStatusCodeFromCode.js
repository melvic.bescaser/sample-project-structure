import { ERROR_STATUS_CODES } from '../config/constants';

const getErrorStatusCodeFromCode = (code) => {
  let statusCode = 500;

  Object.keys(ERROR_STATUS_CODES).some((key) => {
    if (!ERROR_STATUS_CODES[key].includes(code)) {
      return false;
    }

    statusCode = Number(key);

    return true;
  });

  return statusCode;
};

export default getErrorStatusCodeFromCode;
